# FlutterBasic
#properties Container

#properties Stack

#properties Row
chieu ngang         
#properties Column
chieu doc
#userLogin basic
#RxDart
// tạo ra 1 stream và đồng thời emit 1 data là 'gấu bông'
var stream = Stream.value('gấu bông'); 

//Chúng ta sử dụng hàm listen để đăng ký nhận events từ stream.
var stream = Stream.value('gấu bông'); // tạo ra 1 stream và đồng thời emit 1 data là 'gấu bông'
stream.listen((event) { // đăng ký nhận event từ stream
    print('Tôi đã nhận được $event');
  });

Output:
Tôi đã nhận được gấu bông

//Trong RxJava, ..., người ta còn gọi cái lambda này cú pháp : (event) { print('Tôi đã nhận được $event'); } là 1 Observer

//Trong hàm listen chúng ta có thêm các optional parameter như onDone, onError
var stream = Stream.value('gấu bông'); // tạo ra 1 stream và đồng thời emit 1 data là 'gấu bông' và done luôn
  stream.listen((event) {
    print('Tôi đã nhận được $event');
  }, onDone: () => print('Done rồi'), 
      onError: (error) => print('Lỗi rồi $error'));
Output: Do không gặp lỗi nên output sẽ là thế này.
Tôi đã nhận được gấu bông
Done rồi

//Tạo ra một Stream đồng thời emit 1 error và done luôn.
Stream.error(FormatException('lỗi nè')).listen(print, onError: print, onDone: () => print('Done!'))
Output:
FormatException: lỗi nè
Done!

//Tạo ra một Stream đồng thời emit 1 List data xong rồi Done.
Stream.fromIterable([1, 2, 3]).listen(print, onError: print, onDone: () => print('Done!'));
Output:
1
2
3
Done!

//Tạo ra một Stream từ 1 Future.
void main() {
  Stream.fromFuture(testFuture()).listen(print, onError: print, onDone: () => print('Done!'));
}

Future<int> testFuture() async {
  return 3;
}
Output:
3
Done!


Hàm listen ở trên sẽ trả về một đối tượng StreamSubscription. StreamSubscription giúp ta điều khiển stream. Ta có thể pause stream, resume stream và hủy stream.

****NHOM FUNCTION TRA VE FUTURE****
1.elementAt
Hàm này giúp ta get được phần tử tại index bất kỳ trong Stream

void main() async {
  var future = await testStream().elementAt(2); // get data tại index = 2
  print(future); // print ra: 15
}

// xuyên suốt các ví dụ mình sẽ sử dụng Stream này
Stream<int> testStream() async* {
  yield 5;
  yield 10;
  yield 15;
  yield 20;
}


note:
Stream Controlller
StreamController đơn giản chỉ là 1 controller bên trong có 1 stream và controller này giúp ta điều khiển stream đó dễ dàng. Chúng ta có thể thoải mái push các events vào stream, lắng nghe nhận data từ stream đó. Nói cách khác, đây là một con đường khác giúp ta tạo ra 1 Stream và thoải mái emit events và nhận events bất cứ thời điểm nào chúng ta muốn.

Trong mỗi StreamController đều có 2 đối tượng là sink(giúp chúng ta push events đến stream) và stream (giúp chúng ta nhận events từ sink)

//Hàm clear() giống hàm dispose() đều là để cancel tất cả subscription

